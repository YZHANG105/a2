/*
 * BinaryHeap.c
 *
 *  Created on: Feb 9, 2017
 *      Author: cheny_000
 */
#include "BinaryHeap.h"

void initBinarySearchTree(int maxNumberTasks)
{
	int index = 0;

	binaryheap.numberOfElements = 0;
	binaryheap.maxNumberOfTasks = maxNumberTasks;

	OS_TCB **nodeList = binaryheap.nodeList;

	while(index < binaryheap.maxNumberOfTasks)
	{
		nodeList[index] = 0;
		index++;
	}
}

void insertNode_BinaryTree(OS_TCB* _tcb)
{

	//Get the current number of Nodes
	int currentNumber = binaryheap.numberOfElements;

        binaryheap.nodeList[currentNumber] = _tcb;

        
	//Update the new number of elements in the heap
	currentNumber++;
	binaryheap.numberOfElements = currentNumber;
        rearrange();
}

void rearrange()
{

	//Get the node lists from the binaryHeap
	OS_TCB **tasklist = binaryheap.nodeList;

	//Get the current number of Nodes
	int currentNumber = binaryheap.numberOfElements;

	int index = 0;
	while(index < currentNumber)
	{
		heapify(index,tasklist);
		index++;
	}
}

void heapify(int index,OS_TCB **tasklist)
{
	if(index >= 5)
	{
		return;
	}

	//Getting index number

	int IndexCurrentParent = index;
	int IndexLeftChild = index*2 + 1;
	int IndexRightChild =  index*2 + 2;


	//Getting Prio number
       
        int PrioCurrentParent = tasklist[IndexCurrentParent]->period;
	int PrioLeftChild;
	int PrioRightChild;

	if(tasklist[IndexLeftChild] != 0)
	{

		heapify(IndexLeftChild,tasklist);
		PrioLeftChild = tasklist[IndexLeftChild]->period;

		if(tasklist[IndexRightChild] != 0)
		{
			PrioRightChild = tasklist[IndexRightChild]->period;

			if(PrioLeftChild < PrioRightChild)
			{
				if(PrioLeftChild > PrioCurrentParent)
				{
					swap(IndexLeftChild,IndexCurrentParent,tasklist);
				}
			}
			else if(PrioRightChild < PrioLeftChild)
			{
				if(PrioRightChild < PrioCurrentParent)
				{
					swap(IndexRightChild,IndexCurrentParent,tasklist);
				}
			}
		}
		else
		{
			if(PrioLeftChild < PrioCurrentParent)
			{
				swap(IndexLeftChild,IndexCurrentParent,tasklist);
			}
		}
	}
	else
	{
		return;
	}

	if(tasklist[IndexRightChild] != 0)
	{
		heapify(IndexRightChild,tasklist);
	}
}

void swap(int index1,int index2,OS_TCB **tasklist)
{
	OS_TCB *temp = tasklist[index1];
	tasklist[index1] = tasklist[index2];
	tasklist[index2] = temp;
}

void deleteNode_BinaryTree(OS_TCB* _tcb)
{
	//Get the node lists from the binaryHeap
	OS_TCB **tasklist = binaryheap.nodeList;

	//Get the current number of Nodes
	int currentNumber = binaryheap.numberOfElements;

	int index = 0;
        
        while(index < currentNumber)
        {
            if(tasklist[index] == _tcb)
            {
              break;
            }
            index++;
        }
        
	tasklist[index] = 0;
	while(index < currentNumber-1)
	{
                  
		tasklist[index] = tasklist[index+1];
		index++;
	}
	tasklist[index] = 0;
	currentNumber--;
	binaryheap.numberOfElements = currentNumber;

	rearrange();

}

OS_TCB* peekRootNode()
{
	//Get the node lists from the binaryHeap
	OS_TCB **tasklist = binaryheap.nodeList;
	OS_TCB* root = tasklist[0];

	return root;
}

OS_TCB* popRootNode()
{
	OS_TCB* root = peekRootNode();
	deleteNode_BinaryTree(0);
	return root;
}
/*
void printBinarySearchTree()
{
	int index = 0;

	//Get the current number of Nodes
	int currentNumber = binaryheap.numberOfElements;

	//Get the node lists from the binaryHeap
	OS_TCB **tasklist = binaryheap.nodeList;

	while(index < currentNumber)
	{
		OS_TCB *currentTCB = tasklist[index];
		//printf("TCBID = %d, Prio = %d\n",currentTCB->TCBID,currentTCB->period);
		index++;
	}
}
*/
