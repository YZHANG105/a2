/*
*********************************************************************************************************
*
*                                            EXAMPLE CODE
*
* Filename      : OS_RecTask.h
* Version       : V1.00
* Programmer(s) : Chen Yu Hang
*
*********************************************************************************************************
*/

/*
************************************************************************************************************************
*                                                 INCLUDE HEADER FILES
************************************************************************************************************************
*/
#include <os.h>
/*
************************************************************************************************************************
*                                                 DATA STRUCTURE
************************************************************************************************************************
*/

/*
************************************************************************************************************************
*                                                 Binary Heap
************************************************************************************************************************
*/
typedef struct _binaryNode{
    OS_TCB* ptr_tcb;
}BinaryNode;


typedef struct _binaryTree{
    BinaryNode *binaryReadyLists;
    BinaryNode *rootNode;
    CPU_INT08U curNumberOfElements;
    CPU_INT08U maxNumberOfElements;
}BinaryTree;



/*
************************************************************************************************************************
*                                                 FUNCTION PROTOTYPE
************************************************************************************************************************
*/

//Allocate memory block and initialize
void BinaryHeap_init(void);
//Insert Task TCB into the binary heap
void BinaryHeap_Insert(OS_TCB *_ptr_tcb);
//Heapify the structure
void BinaryHeap_Heapify(void);

//print trees
void BinaryHeap_printTree(void);

//delete Nodes
void BinaryHeap_deleteNode(OS_TCB *ptr_tcb);

//Pop the minimum node and heapify
OS_TCB* BinaryHeap_popMinimum();



//Helper Function
CPU_INT08U BinaryHeap_search(OS_TCB *ptr_tcb);             //Return -1 if not found
CPU_INT08U BinaryHeap_leftNodeIndex(CPU_INT08U index);
CPU_INT08U BinaryHeap_rightNodeIndex(CPU_INT08U index);
void BinaryHeap_heapify(CPU_INT08U index,BinaryNode *lists);
void BinaryHeap_swap(CPU_INT08U index1,CPU_INT08U index2);

/*
************************************************************************************************************************
*                                                 RED BLACK TREE
************************************************************************************************************************
*/

typedef struct _OS_USER_TCB{
   OS_TCB         *p_tcb;
   CPU_CHAR       *p_name;
   OS_TASK_PTR     p_task;
   void           *p_arg;
   OS_PRIO         prio;
   CPU_STK        *p_stk_base;
   CPU_STK_SIZE    stk_limit;
   CPU_STK_SIZE    stk_size;
   OS_MSG_QTY      q_size;
   OS_TICK         time_quanta;
   void            *p_ext;
   OS_OPT          opt;
   OS_ERR          *p_err;
   CPU_INT32U      timeRemaining;
   CPU_INT32U      period;
}OS_USER_TCB;


typedef struct _redblackNode{
    OS_USER_TCB* ptr_tcb;
    CPU_INT08U  color;  //0 for red, 1 for black
    struct _redblackNode *left;
    struct _redblackNode *right;
    struct _redblackNode *parent;   
}RedBlackNode;


typedef struct _redblacktree{
    RedBlackNode *rootNode;
    RedBlackNode *neel;
    CPU_INT08U curNumberOfElements;
    CPU_INT08U maxNumberOfElements;
}RedBlackTree;

/*
************************************************************************************************************************
*                                                 FUNCTION PROTOTYPE
************************************************************************************************************************
*/
  
//Allocate memory block and initialize
void RedBlackTree_init(void);

//Red Black Tree insertion
void RedBlackTree_insert(OS_USER_TCB *);

//Red Black Tree deletion
void RedBlackTree_delete(OS_USER_TCB *);

//print RedBlackTree in the pre-order manner
void RedBlackTree_printTree(void);


//Update timeRemaining
void RedBlackTree_updateTime(void);


/*
* Helper function
*/

RedBlackNode *BSTFindSlots(OS_USER_TCB*);
void  BSTRecursiveFinding(RedBlackNode *);
void RedBlackTree_LeftRotate(RedBlackNode *);
void RedBlackTree_RightRotate(RedBlackNode *);
void RedBlackTree_InsertFix(RedBlackNode *);
void RedBlackTree_DeleteFix(RedBlackNode *);
RedBlackNode* search(OS_USER_TCB *);
RedBlackNode* Successor(RedBlackNode*);
RedBlackNode* LeftMost(RedBlackNode*);

/*
************************************************************************************************************************
*                                                 Recursive Task API
************************************************************************************************************************
*/

//Initialize RedBlackTree and Binary Heap
//RedBlackTree is used for recursive data structure
//BinaryHeap is used for Ready List
void OSRec_Init(void);




void  OSRec_TaskCreate (OS_TCB        *p_tcb,
                    CPU_CHAR      *p_name,
                    OS_TASK_PTR    p_task,
                    void          *p_arg,
                    OS_PRIO        prio,
                    CPU_STK       *p_stk_base,
                    CPU_STK_SIZE   stk_limit,
                    CPU_STK_SIZE   stk_size,
                    OS_MSG_QTY     q_size,
                    OS_TICK        time_quanta,
                    void          *p_ext,
                    OS_OPT         opt,
                    OS_ERR        *p_err,
                    CPU_INT32U     remainingTime,
                    CPU_INT32U     period);

void  OSRec_TaskCBinaryTreeCreate (OS_TCB        *p_tcb,
                    CPU_CHAR      *p_name,
                    OS_TASK_PTR    p_task,
                    void          *p_arg,
                    OS_PRIO        prio,
                    CPU_STK       *p_stk_base,
                    CPU_STK_SIZE   stk_limit,
                    CPU_STK_SIZE   stk_size,
                    OS_MSG_QTY     q_size,
                    OS_TICK        time_quanta,
                    void          *p_ext,
                    OS_OPT         opt,
                    OS_ERR        *p_err);


void OSRec_TaskDel(OS_TCB* ptr_tcb);

//Scheduler Global variable

#define  AppTaskTaskDispatcher_STK_SIZE                  128u

static  OS_TCB       AppTaskTaskDispatcherTCB;
static  CPU_STK      AppTaskTaskDispatcherStk[AppTaskTaskDispatcher_STK_SIZE];
static void          leftLEDBlink(void  *p_arg);

//Rec Task scheduler
void OSRec_start(void);




