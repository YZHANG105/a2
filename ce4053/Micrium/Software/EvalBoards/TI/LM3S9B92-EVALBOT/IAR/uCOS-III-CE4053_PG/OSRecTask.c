/*
*********************************************************************************************************
*
*                                            EXAMPLE CODE
*
* Filename      : OS_RecTask.c
* Version       : V1.00
* Programmer(s) : Chen Yu Hang
*
*********************************************************************************************************
*/

/*
************************************************************************************************************************
*                                                 INCLUDE HEADER FILES
************************************************************************************************************************
*/
#include "OSRecTask.h"

/*
************************************************************************************************************************
*                                                 GLOBAL VARIABLE
************************************************************************************************************************
*/


//Global Variable

//Binary Heap
//For Heap Node
OS_MEM   binaryHeapNodeMEM;
BinaryNode      alloMemHeapNode[20];   //1 row for 20 words of BinaryNode

//For Heap Tree
OS_MEM   binaryHeapTreeMEM;
BinaryTree      alloMemHeapTree[1];    //1 row for 1 word of BinaryTree

//Binary Heap rdyList reference
BinaryTree *binaryTree = (BinaryTree *)0;

//Red Black Tree
//Red Black Tree Node
OS_MEM            redblacktreeNodeMEM;
RedBlackNode      alloMemredblackNode[40];   //1 row for 40 words of RedBlackNode

//Red Black Tree
OS_MEM            redblackTreeMEM;
RedBlackTree      alloMemredblackTree[1];    //1 row for 1 word of RedBlackTree

//Red Black recursive List reference
RedBlackTree *redBlackTree = (RedBlackTree *)0;


//OS_USER_TCB
OS_MEM            OS_USER_TCB_MEM;
OS_USER_TCB       alloMemOS_USER_TCB[40];

CPU_INT32U counterT = 0;
/*
************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************
*/

/*
************************************************************************************************************************
*                                                 Binary Heap
************************************************************************************************************************
*/

//Allocate Memory
void BinaryHeap_init(void)
{
    //Allocate Memory for Binary Nodes
    
    OS_ERR err;
     
    printf("Initializing Binary Heap\n");
    //Allocate Memory to binary heap nodes
    OSMemCreate((OS_MEM         *)&binaryHeapNodeMEM,
                (CPU_CHAR       *)"BinaryNodePartition",
                (void           *)&alloMemHeapNode[0],
                (OS_MEM_QTY      ) 20,
                (OS_MEM_SIZE     ) sizeof(BinaryNode),
                (OS_ERR         *)&err);
    
    
    //Allocate Memory to binary heap tree
    OSMemCreate((OS_MEM         *)&binaryHeapTreeMEM,
                (CPU_CHAR       *)"BinaryTreePartition",
                (void           *)&alloMemHeapTree[0],
                (OS_MEM_QTY      ) 2,
                (OS_MEM_SIZE     ) sizeof(BinaryTree),
                (OS_ERR         *)&err);
    

    //Initialize nodelists
    binaryTree = (BinaryTree*)OSMemGet(
                                      (OS_MEM  *)&binaryHeapTreeMEM, 
                                      (OS_ERR  *)&err);
    
    binaryTree->rootNode = (BinaryNode*)0;

    binaryTree->binaryReadyLists = &alloMemHeapNode[0];
  
    int index = 0;
  
    while(index < 20)
    {     
         alloMemHeapNode[index].ptr_tcb =  (OS_TCB*)0;
         index++;
    }

    binaryTree->curNumberOfElements = (CPU_INT08U) 0;
    binaryTree->maxNumberOfElements = (CPU_INT08U) 20;    
}

//Insert Task into the tree
void BinaryHeap_Insert(OS_TCB *_ptr_tcb)
{
    //Gets current number of tasks
    CPU_INT08U currentNumber = binaryTree->curNumberOfElements;
    
    //Added into structure
    alloMemHeapNode[currentNumber].ptr_tcb = _ptr_tcb;
    
    //One more task added into binaryHeap
    currentNumber++;
    
    //Assign back to the structure
    binaryTree->curNumberOfElements = currentNumber;
}

void BinaryHeap_Heapify(void)
{
    //Gets current number of tasks
    CPU_INT08U currentNumber = binaryTree->curNumberOfElements;
    CPU_INT08U index = 0;
    BinaryNode *lists = &alloMemHeapNode[0];
    while(index < currentNumber)
    {
      BinaryHeap_heapify(index,lists);
      index++;
    }
}

//PrintTree
void BinaryHeap_printTree(void)
{
    //Gets current number of tasks
    CPU_INT08U currentNumber = binaryTree->curNumberOfElements;
    CPU_INT08U index = 0;
    BinaryNode *lists = binaryTree->binaryReadyLists;
    while(index < currentNumber)
    {
      printf("%d\n",lists[index].ptr_tcb->period);
      index++;
    }  
}


//Delete particular Nodes
void BinaryHeap_deleteNode(OS_TCB *ptr_tcb)
{
  
  //Get readyList
  BinaryNode *lists = binaryTree->binaryReadyLists;
  //Search index of deleted Node
  CPU_INT08U deletedNode_index = BinaryHeap_search(ptr_tcb);
  //Gets current number of tasks
  CPU_INT08U currentNumber = binaryTree->curNumberOfElements;  
  
  if(deletedNode_index != (CPU_INT08U)255)
  {
    BinaryHeap_swap(deletedNode_index,currentNumber-1);
  }
  
  lists[currentNumber-1].ptr_tcb = (OS_TCB*)0;
  BinaryHeap_Heapify();
  currentNumber--;
  binaryTree->curNumberOfElements = currentNumber;
}

//Pop the minimum node and heapify
OS_TCB* BinaryHeap_popMinimum()
{
  //Get readyList
  BinaryNode *lists = binaryTree->binaryReadyLists;
  OS_TCB* toBeReturned = lists[0].ptr_tcb;
  if(toBeReturned != 0)
  {
    BinaryHeap_deleteNode(toBeReturned);
    return toBeReturned;
  }
  else
  {
    return 0;
  }
}

/*
*  Helper Function
*/

//Return index of Nodes 
CPU_INT08U BinaryHeap_search(OS_TCB *ptr_tcb)
{
    //Gets current number of tasks
    CPU_INT08U currentNumber = binaryTree->curNumberOfElements;
    CPU_INT08U index = 0;
    BinaryNode *lists = binaryTree->binaryReadyLists;
    while(index < currentNumber)
    {
      if(lists[index].ptr_tcb == ptr_tcb)
      {
        return index;
      }
      index++;
    }    
    return 255;
}

//Return LeftNodeIndex
CPU_INT08U BinaryHeap_leftNodeIndex(CPU_INT08U index)
{
    return index * 2 + 1;
}
  
//Return RightNodeIndex
CPU_INT08U BinaryHeap_rightNodeIndex(CPU_INT08U index)
{
    return index * 2 + 2;           
}

//Recursiviliy Heapify
void BinaryHeap_heapify(CPU_INT08U index,BinaryNode *lists)
{
   if(index > 20)
      return;
   
   CPU_INT08U current_Index = index;
   CPU_INT08U left_Index = BinaryHeap_leftNodeIndex(index);
   CPU_INT08U right_Index = BinaryHeap_rightNodeIndex(index);
   
   //if left node is valid
    if(left_Index < 20 && lists[left_Index].ptr_tcb != (OS_TCB *)0)
   {
     //Coniture go to the leftmost
     BinaryHeap_heapify(left_Index,lists);
     
     //If right is valid
      if(right_Index < 20 && lists[right_Index].ptr_tcb != (OS_TCB *)0)
     {
         if(lists[left_Index].ptr_tcb->period < lists[right_Index].ptr_tcb->period)
        {
           if(lists[left_Index].ptr_tcb->period < lists[current_Index].ptr_tcb->period)
          {
            BinaryHeap_swap(left_Index,current_Index);
          }
        }
        else
        {
           if(lists[right_Index].ptr_tcb->period < lists[current_Index].ptr_tcb->period)
          {
            BinaryHeap_swap(right_Index,current_Index);
          }         
        }
     }
     else
     {
       return;
     }
   }
   
   if(right_Index < 20)
   {
     BinaryHeap_heapify(right_Index,lists);
   }
}

//Swap the contents
void BinaryHeap_swap(CPU_INT08U index1,CPU_INT08U index2)
{
   OS_TCB *temp =  binaryTree->binaryReadyLists[index1].ptr_tcb;
   binaryTree->binaryReadyLists[index1].ptr_tcb =  binaryTree->binaryReadyLists[index2].ptr_tcb;
   binaryTree->binaryReadyLists[index2].ptr_tcb = temp;
}

/*
************************************************************************************************************************
*                                                 RED BLACK TREE
************************************************************************************************************************
*/
/*
************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************
*/

void RedBlackTree_init(void)
{
    //Allocate Memory for Binary Nodes
    
    OS_ERR err;
     
    printf("Initializing Red Black Tree\n");
    //Allocate Memory to binary heap nodes
    OSMemCreate((OS_MEM         *)&redblacktreeNodeMEM,
                (CPU_CHAR       *)"RedBlackTreeNodePartition",
                (void           *)&alloMemredblackNode[0],
                (OS_MEM_QTY      ) 40,
                (OS_MEM_SIZE     ) sizeof(RedBlackNode),
                (OS_ERR         *)&err);
    
    printf("Red Black Tree node memory allocated success\n");
    
    //Allocate Memory to binary heap tree
    OSMemCreate((OS_MEM         *)&redblackTreeMEM,
                (CPU_CHAR       *)"RedBlackTreePartition",
                (void           *)&alloMemredblackTree[0],
                (OS_MEM_QTY      ) 2,
                (OS_MEM_SIZE     ) sizeof(RedBlackTree),
                (OS_ERR         *)&err);
    
        //Allocate Memory to OS_USER_TCB
    OSMemCreate((OS_MEM         *)&OS_USER_TCB_MEM,
                (CPU_CHAR       *)"OS_USER_TCB",
                (void           *)&alloMemOS_USER_TCB[0],
                (OS_MEM_QTY      ) 40,
                (OS_MEM_SIZE     ) sizeof(OS_USER_TCB),
                (OS_ERR         *)&err);    
    
    
    //Initialize nodelists
    redBlackTree = (RedBlackTree*)OSMemGet(
                                      (OS_MEM  *)&redblackTreeMEM, 
                                      (OS_ERR  *)&err);
    
    redBlackTree->rootNode = (RedBlackNode*)0;
    redBlackTree->neel =  (RedBlackNode *)OSMemGet(&redblacktreeNodeMEM,
                                         &err);
    redBlackTree->neel->color = 1       ;
    
    printf("Red Black Tree memory allocated success\n");
}

//BST insertion
void RedBlackTree_insert(OS_USER_TCB * _ptr_tcb)
{
  OS_ERR err;
  
  RedBlackNode *y = redBlackTree->neel;
  RedBlackNode *x = redBlackTree->rootNode;
  RedBlackNode *newNode = (RedBlackNode *)OSMemGet(&redblacktreeNodeMEM,
                                         &err);

  if(err == OS_ERR_NONE)
  {
    //printf("Node %d with time %d Created\n",_ptr_tcb->period,_ptr_tcb->timeRemaining);
  }else
  {
    //printf("Node %d with time %d not Created\n",_ptr_tcb->period,_ptr_tcb->timeRemaining); 
  }
  
  newNode->ptr_tcb = _ptr_tcb;
  
  if(redBlackTree->rootNode != (RedBlackNode*)0)
  {
    while(x != redBlackTree->neel)
    {
      y = x;
      if(newNode->ptr_tcb->timeRemaining <= x->ptr_tcb->timeRemaining)
      {
        x = x->left;
      }
      else
      {
        x = x->right;      
      }
    }
  }
  newNode->parent = y;
  
  if(y == redBlackTree->neel)
  {
    redBlackTree->rootNode = newNode;    
  }
  else if(newNode->ptr_tcb->timeRemaining <= y->ptr_tcb->timeRemaining)
  {
    y->left = newNode;    
  }
  else
  {
    y->right = newNode;
  }
  
  newNode->left = redBlackTree->neel;
  newNode->right = redBlackTree->neel;
  newNode->color = (CPU_INT08U)0;
    
  RedBlackTree_InsertFix(newNode);
  
    //  printf("In insert, period = %d,time = %d\n",newNode->ptr_tcb->period,
    //         newNode->ptr_tcb->timeRemaining);
  
} 


void RedBlackTree_updateTime(void)
{

  OS_ERR err;
  RedBlackNode *r =  LeftMost(redBlackTree->rootNode);
  OS_USER_TCB *tcb = r->ptr_tcb;
  
  counterT++;
  
  //printf("\nCurrentTime = %d\n",counterT);
  
  while(r->ptr_tcb->timeRemaining <= counterT) 
  {
    
    tcb = r->ptr_tcb;
    //RedBlackTree_printTree(); 
    //printf("Node %d has triggered,time = %d\n",r->ptr_tcb->period,
     //      r->ptr_tcb->timeRemaining);
    
    RedBlackTree_delete(tcb);
    OSRec_TaskCBinaryTreeCreate(
                 (OS_TCB     *) tcb->p_tcb, 
                 (CPU_CHAR   *) tcb->p_name, 
                 (OS_TASK_PTR ) tcb->p_task, 
                 (void       *) tcb->p_arg, 
                 (OS_PRIO     ) tcb->prio, 
                 (CPU_STK    *) tcb->p_stk_base, 
                 (CPU_STK_SIZE) tcb->stk_limit, 
                 (CPU_STK_SIZE) tcb->stk_size, 
                 (OS_MSG_QTY  ) tcb->q_size, 
                 (OS_TICK     ) tcb->time_quanta, 
                 (void       *) (CPU_INT32U) tcb->p_ext, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err
    );    
    BinaryHeap_Insert(tcb->p_tcb); 
    
    //Time End
    OSRec_TaskCreate(
                   (OS_TCB     *) tcb->p_tcb, 
                   (CPU_CHAR   *) tcb->p_name, 
                   (OS_TASK_PTR ) tcb->p_task, 
                   (void       *) tcb->p_arg, 
                   (OS_PRIO     ) tcb->prio, 
                   (CPU_STK    *) tcb->p_stk_base, 
                   (CPU_STK_SIZE) tcb->stk_limit, 
                   (CPU_STK_SIZE) tcb->stk_size, 
                   (OS_MSG_QTY  ) tcb->q_size, 
                   (OS_TICK     ) tcb->time_quanta, 
                   (void       *) (CPU_INT32U) tcb->p_ext, 
                   (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                   (OS_ERR     *)&err,
                   (CPU_INT32U)  (tcb->period + tcb->timeRemaining),
                   (CPU_INT32U)   tcb->period
    );
    
    RedBlackTree_printTree();
    
    OSMemPut((OS_MEM *)&OS_USER_TCB_MEM,
             (void   *) tcb,
             (OS_ERR *) &err); 
    
    r =  LeftMost(redBlackTree->rootNode);
     //printf("\nLeftMost = %d\n",LeftMost(redBlackTree->rootNode)->ptr_tcb->period);      

  }
  
  //RedBlackTree_printTree();  
 
}

//Red Black Tree deletion
void RedBlackTree_delete(OS_USER_TCB *_ptr_tcb)
{
 
  OS_ERR err;
  
  //Check if the node is exist
  RedBlackNode *delete_node = search(_ptr_tcb);
  
  if(delete_node == (RedBlackNode*)0)
  {
    return;
  }
  
  //To be deleted and return memory 
  RedBlackNode *y = (RedBlackNode*)0;
  //The child to be deleted
  RedBlackNode *x = (RedBlackNode*)0;

  if(delete_node->left == redBlackTree->neel || delete_node->right == redBlackTree->neel)
  {
    y = delete_node;    
  }
  else
  {
    y = Successor(delete_node);
  }

  if(y->left != redBlackTree->neel)
  {
    x = y->left;
  }
  else
  {
    x = y->right;   
  }
  
  x->parent = y->parent;
  
  if(y->parent == redBlackTree->neel)
  {
    redBlackTree->rootNode = x;    
  }
  else if(y == y->parent->left)
  {
    y->parent->left = x;    
  }
  else
  {
   y->parent->right = x;     
  }
  
  if(y != delete_node)
  {
    delete_node->ptr_tcb = y->ptr_tcb;
  }
  
  if(y->color ==1)
  {
    RedBlackTree_DeleteFix(x);
  }
  
  //Returning memory 
   

  
  OSMemPut((OS_MEM *)&redblacktreeNodeMEM,
           (void   *) y,
           (OS_ERR *) &err);
  
}

//print RedBlackTree in the pre-order manner
void RedBlackTree_printTree(void)
{
  //Get Root Node
  RedBlackNode *root = redBlackTree->rootNode;  
  
  //Init Pointer
  
  RedBlackNode *temp = root;
  
  BSTRecursiveFinding(temp);
}

//Helper function

RedBlackNode* LeftMost(RedBlackNode* current)
{
  while(current->left != redBlackTree->neel)
  {
    current = current->left; 
  }
  return current;
}

RedBlackNode* Successor(RedBlackNode* current)
{
  if(current->right != redBlackTree->neel)
  {
    return LeftMost(current->parent);   
  }
  
  RedBlackNode *newNode = current->parent;
  
  while(newNode != redBlackTree->neel && current == newNode->right)
  {
    current = newNode;
    newNode = newNode->parent;    
  }
  return newNode;
}


//Fix the tree after insertion
void RedBlackTree_InsertFix(RedBlackNode *current)
{
  //case 0: if parent is black, no while loop
  
  while(current->parent->color == 0) //if parent is red, go into loop
  {
    //if parent is grandparent's left child
    
    if(current->parent == current->parent->parent->left)
    {
      RedBlackNode *uncle = current->parent->parent->right;
      
      //case 1
      if(uncle->color == 0)
      {
        current->parent->color = 1;
        uncle->color = 1;
        current->parent->parent->color = 0;
        current = current->parent->parent;        
      }
      //case 2 & 3 : uncle is black
      else
      {
        //case 2
        if(current == current->parent->right)
        {
          current = current->parent;
          RedBlackTree_LeftRotate(current);
        }
        //case 3
        current->parent->color = 1;                      //parent -> black
        current->parent->parent->color = 0;              //grandparent -> red
        RedBlackTree_RightRotate(current->parent->parent);
      }
    }
    //if parent is grandparent's right child
    else
    {
      RedBlackNode *uncle = current->parent->parent->left;
      
      //case 1
      if(uncle->color == 0)
      {
        current->parent->color = 1;
        uncle->color = 1;
        current->parent->parent->color = 0;
        current = current->parent->parent;        
      }
      //case 2 & 3 : uncle is black
      else
      {
        //case 2
        if(current == current->parent->left)
        {
          current = current->parent;
          RedBlackTree_RightRotate(current);
        }
        //case 3
        current->parent->color = 1;                      //parent -> black
        current->parent->parent->color = 0;              //grandparent -> red
        RedBlackTree_LeftRotate(current->parent->parent);
      }      
    }
  }
  redBlackTree->rootNode->color = 1;
}

//Fixed the Red Black Tree after deletion
void RedBlackTree_DeleteFix(RedBlackNode *current)
{
  //case 0: if current is red, change it to black 
  //        if current is root, change it to black
  
  while(current != redBlackTree->rootNode && current->color == 1)
  {
    //if current is leftchild
    if(current == current->parent->left)
    {
      RedBlackNode *sibling = current->parent->right;
      
      //case 1: if sibling is red
      if(sibling->color == 0)
      {
        sibling->color = 1;
        current->parent->color = 0;
        RedBlackTree_LeftRotate(current->parent);
        sibling = current->parent->right;        
      }
      
      //Enter case 2, 3 and 4 where sibling is black
      //case 2: if the two childs of sibling are black
      if(sibling->left->color == 1 && sibling->right->color ==1)
      {
        sibling->color = 0;
        //if current is updated to be root, exit the while loop
        current = current->parent;        
      }
      
      //case 3 and 4: there is only one child of current is black
      else
      {
        //case 3: sibling's right child is black and left child is red
        if(sibling->right->color == 1)
        {
            sibling->left->color = 1;
            sibling->color = 0;
            RedBlackTree_RightRotate(sibling);
            sibling = current->parent->right;
        }
        
        //case 4: sibling's left child is black and right child is black
        sibling->color = current->parent->color;
        current->parent->color = 1;
        sibling->right->color = 1;
        RedBlackTree_LeftRotate(current->parent);
        current = redBlackTree->rootNode;       //put current as rootnode and jump out of while loop
      }
    }
    //current is right child
    else
    {
      RedBlackNode *sibling = current->parent->left;
      //case 1: if sibling is red
      if(sibling->color == 0)
      {
        sibling->color = 1;
        current->parent->color = 0;
        RedBlackTree_RightRotate(current->parent);
        sibling = current->parent->left;        
      }
      //Enter case 2, 3 and 4 where sibling is black
      if(sibling->left->color == 1 && sibling->right->color == 1)
      {
        sibling->color = 0;
        current = current->parent;
      }
      else
      {
        //case 3: sibling's left child is black and right child is red
        if(sibling->left->color == 1)
        {
          sibling->right->color = 1;
          sibling->color = 0;
          RedBlackTree_LeftRotate(sibling);
          sibling = current->parent->left;  
        }
        //case 4: sibling's left child is red and right child is black
        sibling->color = current->parent->color;
        current->parent->color = 1;
        sibling->left->color = 1;
        RedBlackTree_RightRotate(current->parent);
        current = redBlackTree->rootNode;       //put current as rootnode and jump out of while loop
      }  
    }    
  }
  current->color = 1; 
}
              

//left rotate about the node
void RedBlackTree_LeftRotate(RedBlackNode *x)
{
   RedBlackNode *y = x->right;
   
   x->right = y->left;
   
   if(y->left != redBlackTree->neel)
   {
     y->left->parent = x;
   }
   
   y->parent = x->parent;
   
   if(x->parent == redBlackTree->neel)
   {
     redBlackTree->rootNode = y;
   }
   else if(x == x->parent->left)
   {
     x->parent->left = y;
   }
   else
   {
     x->parent->right = y;     
   }
   
   y -> left = x;
   x-> parent = y;
}
//right rotate about the node
void RedBlackTree_RightRotate(RedBlackNode *y)
{
  RedBlackNode *x = y->left;
  
  y->left = x->right;
  
  if(x->right != redBlackTree->neel)
  {
    x->right->parent = y;
  }
  
  x->parent = y->parent;
  
  if(y->parent == redBlackTree->neel)
  {
    redBlackTree->rootNode = x;
  }
  else if(y == y->parent->left)
  {
    y->parent->left = x;    
  }
  else
  {
    y->parent->right = x;    
  }
  
  x->right  = y;
  y->parent = x;
}

//Recursively printing nodes
void  BSTRecursiveFinding(RedBlackNode *curNode)
{
  if(curNode == redBlackTree->neel)
  {
      return;
  }
  
  //printf("Node: %d,%d\n",curNode->ptr_tcb->period,
   //      curNode->ptr_tcb->timeRemaining);
  
  BSTRecursiveFinding(curNode->left);
  
  BSTRecursiveFinding(curNode->right);
}


//Get the avaliable leaf node based on ptr tcb
RedBlackNode *BSTFindSlots(OS_USER_TCB* _ptr_tcb)
{
  //Get Root Node
  RedBlackNode *root = redBlackTree->rootNode;
  
  RedBlackNode *temp = (RedBlackNode*)0;
  
  //Root is null
  if(root == (RedBlackNode*)0)
  {
    return (RedBlackNode*)0;    
  }
  
  //start from root
  temp = root;
  
  //Break while loop if the current temp is the leaf node
  while( (temp->left  != redBlackTree->neel) &&
         (temp->right != redBlackTree->neel))
  {
    if(_ptr_tcb->timeRemaining > temp->ptr_tcb->timeRemaining)
    {
      temp = temp->right;      
    }
    else
    {
      temp = temp->left;
    }
  }  
  
  return temp;
}


RedBlackNode* search(OS_USER_TCB *_ptr_tcb)
{
  RedBlackNode *temp = redBlackTree->rootNode;
  
  RedBlackNode *toBeReturned = (RedBlackNode*)0;
  
  while(temp != redBlackTree->neel)
  {
    
    if(temp->ptr_tcb->period == _ptr_tcb->period && 
       temp->ptr_tcb->timeRemaining == _ptr_tcb->timeRemaining)
    {
       //printf("In deletion, period = %d,time = %d\n",temp->ptr_tcb->period,
      //       temp->ptr_tcb->timeRemaining);
      toBeReturned = temp;
      break;      
    }   
    
    if(_ptr_tcb->timeRemaining > temp->ptr_tcb->timeRemaining)
    {
      temp = temp->right;      
    }
    else
    {
      temp = temp->left;
    }
   
  }  
    
  return toBeReturned;
}
                 
/*
************************************************************************************************************************
*                                                 Recursive Task API
************************************************************************************************************************
*/

/*
************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************
*/


//Initialize RedBlackTree and Binary Heap
void OSRec_Init(void)
{
  //ReadyList
  BinaryHeap_init();
  //Initialize
  RedBlackTree_init();
}

//Create recursive Task Node
void  OSRec_TaskCreate (OS_TCB        *_p_tcb,
                        CPU_CHAR      *_p_name,
                        OS_TASK_PTR    _p_task,
                        void          *_p_arg,
                        OS_PRIO        _prio,
                        CPU_STK       *_p_stk_base,
                        CPU_STK_SIZE   _stk_limit,
                        CPU_STK_SIZE   _stk_size,
                        OS_MSG_QTY     _q_size,
                        OS_TICK        _time_quanta,
                        void          *_p_ext,
                        OS_OPT         _opt,
                        OS_ERR        *_p_err,
                        CPU_INT32U     _timeRemaining,
                        CPU_INT32U     _period)
{
   OS_ERR err;
   
   OS_USER_TCB *os_user_tcb = (OS_USER_TCB *)OSMemGet(&OS_USER_TCB_MEM,
                                         &err);
    
   os_user_tcb->p_tcb         = _p_tcb;
   os_user_tcb->p_name        = _p_name; 
   os_user_tcb->p_task        = _p_task;
   os_user_tcb->p_arg         = _p_arg;
   os_user_tcb->prio          = _prio;
   os_user_tcb->p_stk_base    = _p_stk_base;
   os_user_tcb->stk_limit     = _stk_limit;
   os_user_tcb->stk_size      = _stk_size;
   os_user_tcb->q_size        = _q_size;
   os_user_tcb->time_quanta   = _time_quanta;
   os_user_tcb->p_ext         = _p_ext;
   os_user_tcb->opt           = _opt;
   os_user_tcb->p_err         = _p_err;
   os_user_tcb->timeRemaining = _timeRemaining;
   os_user_tcb->period        = _period;
 if(err == OS_ERR_NONE)
  {
    //printf("TCB %d with time %d Created\n",os_user_tcb->period,os_user_tcb->timeRemaining);
  }else
  {
   // printf("TCB %d with time %d not Created\n",os_user_tcb->period,os_user_tcb->timeRemaining); 
  }    
   RedBlackTree_insert(os_user_tcb);
}

//Delete Task From binary Heap
void OSRec_TaskDel(OS_TCB* ptr_tcb)
{
    OS_TCB* binaryHighest = BinaryHeap_popMinimum();
    
    OS_PrioInsert(binaryHighest->Prio);
    OS_RdyListInsertTail(binaryHighest);
}

//Scheduler Task
void scheduler(void)
{
  OS_ERR      err;
  CPU_TS      ts;
  OS_SEM_CTR  ctr;
  
  while(1)
  {
    //Pend
    ctr = OSSemPend(&SwSem,0,OS_OPT_PEND_BLOCKING,&ts,&err);
    //printf("Scheduler triggered\n");
    RedBlackTree_updateTime();
    //RedBlackTree_updateTime();
    //printf("Time Fired\n");
  }
}

//Rec Task scheduler
void OSRec_start(void)
{
    OS_ERR err;
    OSTaskCreate(
                 (OS_TCB     *)&AppTaskTaskDispatcherTCB, 
                 (CPU_CHAR   *)"TaskScheduler", 
                 (OS_TASK_PTR ) scheduler, 
                 (void       *) 0, 
                 (OS_PRIO     ) AppTaskTaskDispatcher_PRIO, 
                 (CPU_STK    *) &AppTaskTaskDispatcherStk[0], 
                 (CPU_STK_SIZE) AppTaskTaskDispatcher_STK_SIZE / 10u, 
                 (CPU_STK_SIZE) AppTaskTaskDispatcher_STK_SIZE, 
                 (OS_MSG_QTY  ) 0u, 
                 (OS_TICK     ) 0u, 
                 (void       *) (CPU_INT32U) 2, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err
    );   
    printf("Recursive Task created\n");
}


//Task Create Called by Binary Tree
void  OSRec_TaskCBinaryTreeCreate (OS_TCB        *p_tcb,
                    CPU_CHAR      *p_name,
                    OS_TASK_PTR    p_task,
                    void          *p_arg,
                    OS_PRIO        prio,
                    CPU_STK       *p_stk_base,
                    CPU_STK_SIZE   stk_limit,
                    CPU_STK_SIZE   stk_size,
                    OS_MSG_QTY     q_size,
                    OS_TICK        time_quanta,
                    void          *p_ext,
                    OS_OPT         opt,
                    OS_ERR        *p_err)
{
    CPU_STK_SIZE   i;
#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    OS_OBJ_QTY     reg_nbr;
#endif
    CPU_STK       *p_sp;
    CPU_STK       *p_stk_limit;
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#ifdef OS_SAFETY_CRITICAL_IEC61508
    if (OSSafetyCriticalStartFlag == DEF_TRUE) {
       *p_err = OS_ERR_ILLEGAL_CREATE_RUN_TIME;
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* ---------- CANNOT CREATE A TASK FROM AN ISR ---------- */
        *p_err = OS_ERR_TASK_CREATE_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u                                  /* ---------------- VALIDATE ARGUMENTS ------------------ */
    if (p_tcb == (OS_TCB *)0) {                             /* User must supply a valid OS_TCB                        */
        *p_err = OS_ERR_TCB_INVALID;
        return;
    }
    if (p_task == (OS_TASK_PTR)0) {                         /* User must supply a valid task                          */
        *p_err = OS_ERR_TASK_INVALID;
        return;
    }
    if (p_stk_base == (CPU_STK *)0) {                       /* User must supply a valid stack base address            */
        *p_err = OS_ERR_STK_INVALID;
        return;
    }
    if (stk_size < OSCfg_StkSizeMin) {                      /* User must supply a valid minimum stack size            */
        *p_err = OS_ERR_STK_SIZE_INVALID;
        return;
    }
    if (stk_limit >= stk_size) {                            /* User must supply a valid stack limit                   */
        *p_err = OS_ERR_STK_LIMIT_INVALID;
        return;
    }
    if (prio >= OS_CFG_PRIO_MAX) {                          /* Priority must be within 0 and OS_CFG_PRIO_MAX-1        */
        *p_err = OS_ERR_PRIO_INVALID;
        return;
    }
#endif

#if OS_CFG_ISR_POST_DEFERRED_EN > 0u
    if (prio == (OS_PRIO)0) {
        if (p_tcb != &OSIntQTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use priority 0                          */
            return;
        }
    }
#endif

    if (prio == (OS_CFG_PRIO_MAX - 1u)) {
        if (p_tcb != &OSIdleTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use same priority as idle task          */
            return;
        }
    }

    OS_TaskInitTCB(p_tcb);                                  /* Initialize the TCB to default values                   */

    *p_err = OS_ERR_NONE;
                                                            /* --------------- CLEAR THE TASK'S STACK --------------- */
    if ((opt & OS_OPT_TASK_STK_CHK) != (OS_OPT)0) {         /* See if stack checking has been enabled                 */
        if ((opt & OS_OPT_TASK_STK_CLR) != (OS_OPT)0) {     /* See if stack needs to be cleared                       */
            p_sp = p_stk_base;
            for (i = 0u; i < stk_size; i++) {               /* Stack grows from HIGH to LOW memory                    */
                *p_sp = (CPU_STK)0;                         /* Clear from bottom of stack and up!                     */
                p_sp++;
            }
        }
    }
                                                            /* ------- INITIALIZE THE STACK FRAME OF THE TASK ------- */
#if (CPU_CFG_STK_GROWTH == CPU_STK_GROWTH_HI_TO_LO)
    p_stk_limit = p_stk_base + stk_limit;
#else
    p_stk_limit = p_stk_base + (stk_size - 1u) - stk_limit;
#endif

    p_sp = OSTaskStkInit(p_task,
                         p_arg,
                         p_stk_base,
                         p_stk_limit,
                         stk_size,
                         opt);

                                                            /* -------------- INITIALIZE THE TCB FIELDS ------------- */
    p_tcb->TaskEntryAddr = p_task;                          /* Save task entry point address                          */
    p_tcb->TaskEntryArg  = p_arg;                           /* Save task entry argument                               */

    p_tcb->NamePtr       = p_name;                          /* Save task name                                         */

    p_tcb->Prio          = prio;                            /* Save the task's priority                               */

    p_tcb->StkPtr        = p_sp;                            /* Save the new top-of-stack pointer                      */
    p_tcb->StkLimitPtr   = p_stk_limit;                     /* Save the stack limit pointer                           */

    p_tcb->TimeQuanta    = time_quanta;                     /* Save the #ticks for time slice (0 means not sliced)    */
#if OS_CFG_SCHED_ROUND_ROBIN_EN > 0u
    if (time_quanta == (OS_TICK)0) {
        p_tcb->TimeQuantaCtr = OSSchedRoundRobinDfltTimeQuanta;
    } else {
        p_tcb->TimeQuantaCtr = time_quanta;
    }
#endif
    p_tcb->ExtPtr        = p_ext;                           /* Save pointer to TCB extension                          */
    p_tcb->StkBasePtr    = p_stk_base;                      /* Save pointer to the base address of the stack          */
    p_tcb->StkSize       = stk_size;                        /* Save the stack size (in number of CPU_STK elements)    */
    p_tcb->Opt           = opt;                             /* Save task options                                      */

#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    for (reg_nbr = 0u; reg_nbr < OS_CFG_TASK_REG_TBL_SIZE; reg_nbr++) {
        p_tcb->RegTbl[reg_nbr] = (OS_REG)0;
    }
#endif

#if OS_CFG_TASK_Q_EN > 0u
    OS_MsgQInit(&p_tcb->MsgQ,                               /* Initialize the task's message queue                    */
                q_size);
#endif

    OSTaskCreateHook(p_tcb);                                /* Call user defined hook                                 */

                                                            /* --------------- ADD TASK TO READY LIST --------------- */
    OS_CRITICAL_ENTER();

#if OS_CFG_DBG_EN > 0u
    OS_TaskDbgListAdd(p_tcb);
#endif

    OSTaskQty++;                                            /* Increment the #tasks counter                           */

    if (OSRunning != OS_STATE_OS_RUNNING) {                 /* Return if multitasking has not started                 */
        OS_CRITICAL_EXIT();
        return;
    }

    OS_CRITICAL_EXIT_NO_SCHED(); 
}

